export class IllegalArgumentError<T> {

  constructor(readonly name: string,
              readonly value: T,
              readonly message: string) {}

}
