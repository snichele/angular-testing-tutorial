import {Identification} from "./Identification";

describe("Les informations d'identification de l'utilisateur", () => {

  describe("sont portées par la classe `Identification`", () => {

    it("que je peux instancier avec un identifiant et un mot de passe conforme", () => {

      const ident = new Identification("azeaze", "aZ1*aZ1*");
      expect(ident).toBeDefined();

    });

    it("que je ne PEUX PAS instancier avec un identifiant ou un mot de passe non-conforme", () => {

      expect(() => new Identification("aze", "aZ1*aZ1*")).toThrow();
      expect(() => new Identification("azazee", "aZ1*aZ1")).toThrow();

    });

  });

});
