import {IllegalArgumentError} from "../errors/IllegalArgumentError";

/** 4 à 16 caractères alphanumériques (ex : azer12) */
export type IdentifiantUtilisateur = string;

/** 8+ caractères, contenant au moins un de chaque : a-z, A-Z, 0-9 et caractères spéciaux
 * ex : azerty123$
 */
export type MotDePasseUtilisateur = string;

/** Porte les informations d'identification d'un utilisateur. */
export class Identification {

  /**
   * @see IdentifiantUtilisateur
   * @see MotDePasseUtilisateur
   * @throws IllegalArgumentError si id ou mot de passe n'est pas conforme
   */
  constructor(readonly id: IdentifiantUtilisateur,
              readonly motDePasse: MotDePasseUtilisateur) {

    if (!new RegExp("^[0-9a-zA-Z]{4,16}$").test(id)) {
      throw new IllegalArgumentError(
        "id",
        id,
        "IdentifiantUtilisateur incorrect : attendu entre 4 et 16 caractères alphanumériques."
      );
    }

    if (!new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})").test(motDePasse)) {
      throw new IllegalArgumentError(
        "motDePasse",
        motDePasse,
        "MotDePasseUtilisateur incorrect : attendu 8 caractères avec lettres minuscules, majuscules, chiffres et caractères spéciaux."
      );
    }

  }

}
