import {AfterViewInit, Component} from "@angular/core";

@Component({
  selector: "login-sample-app",
  template: `
    <div class="body">
      <h1>Angular is ready !</h1>
    </div>
  `,
  styleUrls: ["./login-sample-template.component.scss"]
})
export class LoginSampleTemplateComponent implements AfterViewInit {

  ngAfterViewInit(): void {
  }

}
