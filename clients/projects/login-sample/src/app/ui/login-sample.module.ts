import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";

import {LoginSampleTemplateComponent} from "./login-sample-template.component";

/**
 * Bootstrap the aplc client plugin application.
 */
@NgModule({
  declarations: [
    LoginSampleTemplateComponent
  ],
  imports: [
    BrowserModule,
  ],
  providers: [],
  bootstrap: [LoginSampleTemplateComponent]
})
export class LoginSampleModule {

}
