import {enableProdMode} from "@angular/core";
import {platformBrowserDynamic} from "@angular/platform-browser-dynamic";
import {environment} from "./environments/environment";
import {LoginSampleModule} from "./app/ui/login-sample.module";


if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic()
  .bootstrapModule(
    LoginSampleModule
  )
  .catch(err =>
    console.error(err)
  );


