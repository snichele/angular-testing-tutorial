import {MinutesToHumanReadableTimePipe} from "./MinutesToHumanReadableTimePipe";

describe("The 'MinutesToHumanReadableTimePipe' pipe", () => {

  const pipe = new MinutesToHumanReadableTimePipe();

  describe("permits me to format a duration in minutes into a human readable string", () => {

    describe("when the number of minutes is", () => {

      it("negative", () => {
        expect(pipe.transform(-2)).toEqual("0mn");
      });

      it.todo("less than one hour ( < 60 )");

      it.todo("and don't forget to add all other cases to cover ! :)");

    });

  });

});
