import {Pipe, PipeTransform} from "@angular/core";

/*
 * Convert minutes to a human readable time representation (in french).
 *
 * Ex : 70 => 1h 10mn
 */
@Pipe({name: "mnToHRTime"})
export class MinutesToHumanReadableTimePipe implements PipeTransform {

  transform(value: number): string {
    if (value < 0) {
      return "0mn";
    }
    return value + "mn";
  }

}
