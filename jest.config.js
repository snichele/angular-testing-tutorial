module.exports = {
  "roots": [
    "<rootDir>/clients/projects/"
  ],
  "transform": {
    ".(ts|tsx)": "ts-jest"
  },
  "globals": {
    "ts-jest": {
      "tsconfig": "./tsconfig.spec.json"
    }
  },
  "testRegex": "(.*).spec\\.ts",
  "moduleFileExtensions": [
    "ts",
    "js"
  ],
  "coverageDirectory": "./sonar/coverage/",
  "collectCoverageFrom": [
    "<rootDir>/clients/projects/**/*.ts"
  ],
  "testURL": "http://localhost",
  "testEnvironment": "jest-environment-node",
  "testPathIgnorePatterns": [
    ".git/.*",
    "node_modules/.*"
  ],
  "transformIgnorePatterns": [
    "node_modules/.*",
    ".*\\.js"
  ]
}
